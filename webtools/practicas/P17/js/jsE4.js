function Analizando(){
    var frasem = document.getElementById('form-frase').value;

    //parte 1
    var mitadc = "";
    var c;
    var mitad = parseInt(((frasem.length))/2);
    for(c=0;c<mitad;c++){
        mitadc = mitadc + frasem.charAt(c);
    }

    var iframedoc01 = analizador1.document;

    if(analizador1.contentDocument){
        iframedoc01 = analizador1.contentDocument;
    }else{
        if (analizador1.contentWindow){
            iframedoc01 = analizador1.contentWindow.document;
        }
    }

    if(iframedoc01){
        iframedoc01.open();
        iframedoc01.writeln(mitadc);
        iframedoc01.close();
    }else{
        alert('No es posible insertar el contenido dinámicamente en el iframe.');
    }

    //parte 2
    var ultimoc = frasem.charAt((frasem.length)-1);

    var iframedoc02 = analizador2.document;

    if(analizador2.contentDocument){
        iframedoc02 = analizador2.contentDocument;
    }else{
        if (analizador2.contentWindow){
            iframedoc02 = analizador2.contentWindow.document;
        }
    }

    if(iframedoc02){
        iframedoc02.open();
        iframedoc02.writeln(ultimoc);
        iframedoc02.close();
    }else{
        alert('No es posible insertar el contenido dinámicamente en el iframe.');
    }

    //parte 3
    var inverso = "";
    for(c=((frasem.length)-1);c>=0;c--){
        inverso = inverso + frasem.charAt(c);
    }

    var iframedoc03 = analizador3.document;

    if(analizador3.contentDocument){
        iframedoc03 = analizador3.contentDocument;
    }else{
        if (analizador3.contentWindow){
            iframedoc03 = analizador3.contentWindow.document;
        }
    }

    if(iframedoc03){
        iframedoc03.open();
        iframedoc03.writeln(inverso);
        iframedoc03.close();
    }else{
        alert('No es posible insertar el contenido dinámicamente en el iframe.');
    }

    //parte 4
    var sepun = "";
    for(c=0;c<frasem.length;c++){
        if(frasem.charAt(c)==' '){
            sepun = sepun + '.';
        }else{
            sepun = sepun + frasem.charAt(c)+" . ";
        }
    }

    var iframedoc04 = analizador4.document;

    if(analizador4.contentDocument){
        iframedoc04 = analizador4.contentDocument;
    }else{
        if (analizador4.contentWindow){
            iframedoc04 = analizador4.contentWindow.document;
        }
    }

    if(iframedoc04){
        iframedoc04.open();
        iframedoc04.writeln(sepun);
        iframedoc04.close();
    }else{
        alert('No es posible insertar el contenido dinámicamente en el iframe.');
    }

    //parte5
    var numa = 0;
    for(c=0;c<frasem.length;c++){
        if((frasem.charAt(c)=='a')||(frasem.charAt(c)=='A')){
            numa = parseInt(numa) + 1;
        }
    }

    var iframedoc05 = analizador5.document;

    if(analizador5.contentDocument){
        iframedoc05 = analizador5.contentDocument;
    }else{
        if (analizador5.contentWindow){
            iframedoc05 = analizador5.contentWindow.document;
        }
    }

    if(iframedoc05){
        iframedoc05.open();
        iframedoc05.writeln(numa);
        iframedoc05.close();
    }else{
        alert('No es posible insertar el contenido dinámicamente en el iframe.');
    }
}